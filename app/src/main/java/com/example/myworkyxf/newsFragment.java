package com.example.myworkyxf;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class newsFragment extends Fragment {

    private Context context;

    public newsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view2 = inflater.inflate(R.layout.fragment_news, container, false);
        context = this.getActivity();
        RecyclerView recyclerView2 = view2.findViewById(R.id.recyclerview2);


        String[] friends = new String[15];
        String[] news = new String[15];
        for (int i = 0; i < 15; i++) {
            friends[i] = (i+"号朋友");
            news[i] = ("这是" + i + "号朋友发来的消息");
        }

        List<Map<String, Object>> items2 = new ArrayList<Map<String, Object>>();
        for (int i=0;i<friends.length;i++) {
            Map<String, Object> item2 = new HashMap<String, Object>();
            item2.put("User", friends[i]);
            item2.put("Message", news[i]);
            item2.put("Picture", R.drawable.photo);
            items2.add(item2);
        }

        MyAdapter_news myadapter2 = new MyAdapter_news(items2, context);
        LinearLayoutManager layoutManager=new LinearLayoutManager(this.getActivity());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView2.setLayoutManager(layoutManager);
        recyclerView2.setAdapter(myadapter2);

        return view2;
    }
}