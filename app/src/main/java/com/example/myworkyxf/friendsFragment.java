package com.example.myworkyxf;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.app.Fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class friendsFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<String> data;
    private Context context;
    private Myadapter_friends myadapter;

    public friendsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_friends,container,false);
        context = this.getActivity();
        recyclerView = view.findViewById(R.id.recyclerview);

        String[] friends = new String[15];
        for (int i=0;i<15;i++){
            friends[i]=("这是第"+i+"个朋友");
        }

        List<Map<String,Object>> items=new ArrayList<Map<String,Object>>();
        for(int i=0;i<friends.length;i++) {
            Map<String,Object> item=new HashMap<String,Object>();
            item.put("Friends", friends[i]);
            item.put("Photos", R.drawable.photo);
            items.add(item);
        }

        myadapter=new Myadapter_friends(items,context);
        LinearLayoutManager layoutManager=new LinearLayoutManager(this.getActivity());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(myadapter);
        return view;
    }
}