package com.example.myworkyxf;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Fragment newsFragment=new newsFragment();
    private Fragment discoveryFragment=new discoveryFragment();
    private Fragment friendsFragment=new friendsFragment();
    private Fragment settingFragment=new settingFragment();

    private FragmentManager fragmentManager;

    private LinearLayout linearLayout_news,linearLayout_friends,linearLayout_discovery,linearLayout_setting;

    private ImageView imageView1,imageView2,imageView3,imageView4;

    private TextView textView1,textView2,textView3,textView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        linearLayout_news=findViewById(R.id.linearLayout_news);
        linearLayout_friends=findViewById(R.id.linearLayout_friends);
        linearLayout_discovery=findViewById(R.id.linearLayout_discovery);
        linearLayout_setting=findViewById(R.id.linearLayout_setting);

        linearLayout_news.setOnClickListener(this);
        linearLayout_friends.setOnClickListener(this);
        linearLayout_discovery.setOnClickListener(this);
        linearLayout_setting.setOnClickListener(this);

        imageView1=findViewById(R.id.imageView1);
        imageView2=findViewById(R.id.imageView2);
        imageView3=findViewById(R.id.imageView3);
        imageView4=findViewById(R.id.imageView4);

        textView1=findViewById(R.id.textView1);
        textView2=findViewById(R.id.textView2);
        textView3=findViewById(R.id.textView3);
        textView4=findViewById(R.id.textView4);

        initFragment();
    }

    private void showcolor(int i){
        imageView1.setImageResource(R.drawable.news);
        textView1.setTextColor(Color.BLACK);
        imageView2.setImageResource(R.drawable.friends);
        textView2.setTextColor(Color.BLACK);
        imageView3.setImageResource(R.drawable.discovery);
        textView3.setTextColor(Color.BLACK);
        imageView4.setImageResource(R.drawable.setting);
        textView4.setTextColor(Color.BLACK);
        switch (i){
            case 0:
                imageView1.setImageResource(R.drawable.news1);
                textView1.setTextColor(Color.GREEN);
                break;
            case 1:
                imageView2.setImageResource(R.drawable.friends1);
                textView2.setTextColor(Color.GREEN);
                break;
            case 2:
                imageView3.setImageResource(R.drawable.discovery1);
                textView3.setTextColor(Color.GREEN);
                break;
            case  3:
                imageView4.setImageResource(R.drawable.setting1);
                textView4.setTextColor(Color.GREEN);
                break;
            default:
                break;

        }

    }

    private void initFragment() {
        fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.id_content, newsFragment);
        transaction.add(R.id.id_content, settingFragment);
        transaction.add(R.id.id_content, friendsFragment);
        transaction.add(R.id.id_content, discoveryFragment);
        showcolor(1);
        showfragment(1);
        transaction.commit();

    }

    private void hideFragment(FragmentTransaction transaction) {
        transaction.hide(newsFragment);
        transaction.hide(settingFragment);
        transaction.hide(friendsFragment);
        transaction.hide(discoveryFragment);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.linearLayout_news:
                showfragment(0);
                showcolor(0);
                break;
            case R.id.linearLayout_friends:
                showfragment(1);
                showcolor(1);
                break;
            case R.id.linearLayout_discovery:
                showfragment(2);
                showcolor(2);
                break;
            case R.id.linearLayout_setting:
                showfragment(3);
                showcolor(3);
                break;
            default:
                break;
        }

    }

    private void showfragment(int i) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        hideFragment(transaction);
        switch (i){
            case 0:
                transaction.show(newsFragment);
                break;
            case 1:
                transaction.show(friendsFragment);
                break;
            case 2:
                transaction.show(discoveryFragment);
                break;
            case 3:
                transaction.show(settingFragment);

                break;
            default:
                break;

        }
        transaction.commit();
    }


}